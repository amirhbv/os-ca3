#include <fstream>
#include <sstream>

#include "country.hpp"

const char CITY_DELIMA = '-';

using namespace std;

Country::Country(string inputFile)
{
    ifstream fin(inputFile);
    string line;

    while (getline(fin, line))
    {
        if (line == "#")
        {
            break;
        }
        this->addRoad(line);
    }

    while (getline(fin, line))
    {
        Path *newPath = this->addPath(line);
        getline(fin, line);
        int numberOfCars = stoi(line);
        this->addCars(newPath, numberOfCars);
    }

    fin.close();
}

void Country::addRoad(std::string line)
{
    stringstream sin(line);

    string start, end, hardness;
    getline(sin, start, CITY_DELIMA);
    getline(sin, end, CITY_DELIMA);
    getline(sin, hardness);

    this->roads.push_back(new Road(start, end, stoi(hardness)));
}

Path *Country::addPath(std::string line)
{
    stringstream sin(line);

    vector<string> cities;
    string city;
    while (getline(sin, city, CITY_DELIMA))
    {
        cities.push_back(city);
    }

    vector<Road *> pathRoads;
    for (size_t i = 0; i < cities.size() - 1; i++)
    {
        string start = cities[i];
        string end = cities[i + 1];
        pathRoads.push_back(this->findRoad(start, end));
    }

    Path *path = new Path(pathRoads);
    this->paths.push_back(path);
    return path;
}

Road *Country::findRoad(std::string start, std::string end) const
{
    for (Road *road : this->roads)
    {
        if (road->match(start, end))
        {
            return road;
        }
    }
    return nullptr;
}

void Country::addCars(Path *path, int numberOfCars)
{
    for (int i = 0; i < numberOfCars; i++)
    {
        this->cars.push_back(new Car(i, path));
    }
}

int Country::start()
{
    for (Car *car : this->cars)
    {
        this->carThreads.push_back(thread(&Car::go, car));
    }
    this->joinAllThreads();
    return 0;
}

void Country::joinAllThreads()
{
    for (thread &t : this->carThreads)
    {
        if (t.joinable())
        {
            t.join();
        }
    }
}

Country::~Country()
{
    this->joinAllThreads();
}

EmissionType Country::totalEmission = 0;
Monitor Country::totalEmissionLock;

void Country::addEmission(EmissionType emssion, EmissionType &currentTotalEmission)
{
    Country::totalEmissionLock.wait();

    Country::totalEmission += emssion;
    currentTotalEmission = Country::totalEmission;

    Country::totalEmissionLock.signal();
}
