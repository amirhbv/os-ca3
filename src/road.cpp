#include "road.hpp"

using namespace std;
Road::Road(string start, string end, int hardness)
    : start(start), end(end), hardness(hardness)
{
}

Road::~Road()
{
}

string Road::getStart() const
{
    return this->start;
}

string Road::getEnd() const
{
    return this->end;
}

int Road::getHardness() const
{
    return this->hardness;
}

bool Road::matchStart(string start) const
{
    return this->start == start;
}

bool Road::matchEnd(string end) const
{
    return this->end == end;
}

bool Road::match(string start, string end) const
{
    return this->matchStart(start) && this->matchEnd(end);
}

void Road::acquire()
{
    this->monitor.wait();
}

void Road::release()
{
    this->monitor.signal();
}
