#include "path.hpp"

using namespace std;

int Path::nextId = 1;

Path::Path(vector<Road *> roads)
    : id(nextId++), roads(roads)
{
}

Path::~Path()
{
}

int Path::getId() const
{
    return this->id;
}

std::string Path::getStartCity() const
{
    return this->roads.front()->getStart();
}

bool Path::hasNextCity(std::string currentCity) const
{
    return !this->roads.back()->matchEnd(currentCity);
}

Road *Path::getNextRoad(std::string currentCity) const
{
    for (Road *road : this->roads)
    {
        if (road->matchStart(currentCity))
        {
            return road;
        }
    }
    return nullptr;
}
