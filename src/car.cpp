#include "car.hpp"

#include <cstdlib>
#include <fstream>

#include "road.hpp"
#include "country.hpp"

using namespace std;
using namespace chrono;

Car::Car(int id, Path *path)
    : id(id + 1), path(path)
{
    this->pollutionRatio = rand() % 10 + 1;
    this->currentCity = this->path->getStartCity();
}

Car::~Car()
{
}

EmissionType Car::calculateEmission(Road *currentRoad)
{
    EmissionType denominator = 1e6 * this->pollutionRatio * currentRoad->getHardness();
    EmissionType res = 0;
    for (long long int i = 0; i < 1e7; i++)
    {
        res += i / denominator;
    }
    return res;
}

void Car::go()
{
    while (this->path->hasNextCity(this->currentCity))
    {
        Road *currentRoad = this->path->getNextRoad(this->currentCity);

        currentRoad->acquire();
        milliseconds entranceTime = duration_cast<milliseconds>(system_clock::now().time_since_epoch());

        EmissionType emission = this->calculateEmission(currentRoad);

        milliseconds exitTime = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
        currentRoad->release();

        EmissionType currentTotalEmission;
        Country::addEmission(emission, currentTotalEmission);

        this->saveLogToFile(currentRoad, entranceTime, exitTime, emission, currentTotalEmission);

        this->currentCity = currentRoad->getEnd();
    }
}

void Car::saveLogToFile(Road *currentRoad, milliseconds entranceTime, milliseconds exitTime, EmissionType emission, EmissionType currentTotalEmission)
{
    const string outputDelima = ", ";
    const string outputFileName = "res/" + to_string(this->path->getId()) + "-" + to_string(this->id);

    ofstream fout(outputFileName, ios_base::app);

    fout << currentRoad->getStart() << outputDelima
         << entranceTime.count() << outputDelima
         << currentRoad->getEnd() << outputDelima
         << exitTime.count() << outputDelima
         << to_string(emission) << outputDelima
         << to_string(currentTotalEmission) << endl;

    fout.close();
}
