#include "monitor.hpp"

Monitor::Monitor()
{
    sem_init(&this->mutex, 0, 1);
}
Monitor::Monitor(int initial)
{
    sem_init(&this->mutex, 0, initial);
}

Monitor::~Monitor()
{
    sem_destroy(&this->mutex);
}

void Monitor::wait()
{
    sem_wait(&this->mutex);
}

void Monitor::signal()
{
    sem_post(&this->mutex);
}
