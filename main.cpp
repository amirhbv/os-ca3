#include <iostream>
#include <ctime>
#include <cstdlib>

#include "country.hpp"

using namespace std;

int main(int argc, char const *argv[])
{
    if (argc < 2)
    {
        cout << "too few arguments." << endl;
        exit(EXIT_FAILURE);
    }

    srand(time(NULL));

    system("rm -rf res");
    system("mkdir -p res");

    Country country(argv[1]);
    return country.start();
}
