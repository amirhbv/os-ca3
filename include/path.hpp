#if !defined(PATH_HPP)
#define PATH_HPP

#include <vector>

#include "road.hpp"

class Path
{
private:
    static int nextId;

    int id;
    std::vector<Road *> roads;

public:
    Path(std::vector<Road *>);
    ~Path();

    int getId() const;

    bool hasNextCity(std::string) const;
    Road *getNextRoad(std::string) const;
    std::string getStartCity() const;
};

#endif // PATH_HPP
