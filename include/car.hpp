#if !defined(CAR_HPP)
#define CAR_HPP

#include <string>
#include <chrono>

#include "path.hpp"
#include "emission.hpp"

class Car
{
private:
    int id;
    Path *path;
    int pollutionRatio;
    std::string currentCity;

    EmissionType calculateEmission(Road *);
    void saveLogToFile(Road *, std::chrono::milliseconds, std::chrono::milliseconds, EmissionType, EmissionType);

public:
    Car(int, Path *);
    ~Car();

    void go();
};

#endif // CAR_HPP
