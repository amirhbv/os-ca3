#if !defined(ROAD_HPP)
#define ROAD_HPP

#include <string>

#include "monitor.hpp"

class Road
{
private:
    std::string start, end;
    int hardness;
    Monitor monitor;

public:
    Road(std::string, std::string, int);
    ~Road();

    std::string getStart() const;
    std::string getEnd() const;
    int getHardness() const;

    bool matchStart(std::string) const;
    bool matchEnd(std::string) const;
    bool match(std::string, std::string) const;

    void acquire();
    void release();
};

#endif // ROAD_HPP
