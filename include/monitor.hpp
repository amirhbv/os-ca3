#if !defined(MONITOR_HPP)
#define MONITOR_HPP

#include <semaphore.h>

class Monitor
{
private:
    sem_t mutex;

public:
    Monitor();
    Monitor(int);
    ~Monitor();

    void wait();
    void signal();
};

#endif // MONITOR_HPP
