#if !defined(COUNTRY_HPP)
#define COUNTRY_HPP

#include <string>
#include <thread>
#include <vector>

#include "car.hpp"
#include "road.hpp"
#include "path.hpp"
#include "monitor.hpp"
#include "emission.hpp"

class Country
{
private:
    std::vector<std::thread> carThreads;
    std::vector<Car *> cars;
    std::vector<Road *> roads;
    std::vector<Path *> paths;

    void addRoad(std::string);
    Path *addPath(std::string);
    void addCars(Path *, int);

    Road *findRoad(std::string, std::string) const;

    void joinAllThreads();

    static EmissionType totalEmission;
    static Monitor totalEmissionLock;

public:
    Country(std::string);
    ~Country();

    int start();

    static void addEmission(EmissionType, EmissionType &);
};

#endif // COUNTRY_HPP
